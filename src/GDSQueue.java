import java.util.NoSuchElementException;

/**
 * 
 */

/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class GDSQueue<T> extends GQueue<T> {

    public GDSQueue() {
        list = new GDSList<T>();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#add(java.lang.Object)
     */
    @Override
    public boolean add(T obj) {
        return offer(obj);
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#size()
     */
    @Override
    public int size() {
        return list.size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GQueue#offer(java.lang.Object)
     */
    @Override
    public boolean offer(T t) {
        // Cannot offer null objects
        if (t == null) {
            throw new NullPointerException();
        }
        // Add item to end of list
        return list.add(t);
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GQueue#poll()
     */
    @Override
    public T poll() {
        // Cannot get item when queue is empty
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        
        // Remove the first item in the queue
        return list.remove(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GQueue#peek()
     */
    @Override
    public T peek() {
        // Cannot get item on empty queue
        if (isEmpty()) {
            return null;
        }
        // Return the first item in the queue
        return list.get(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GQueue#toString()
     */
    @Override
    public String toString() {
        return list.toString();
    }

    @Override
    public int hashCode() {
        return 11 * list.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (this.getClass() == o.getClass()) {
            GDSQueue<T> queue = (GDSQueue<T>) o;
            return this.list.equals(queue.list);
        }
        return false;
    }

}
