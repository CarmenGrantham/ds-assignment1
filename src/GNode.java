/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class GNode<T> {

    public GNode<T> next = null;
    public GNode<T> previous = null;
    private T obj = null;

    public GNode(GNode<T> next_, GNode<T> prev_, T object_) {
        this.next = next_;
        this.previous = prev_;
        this.obj = object_;
    }

    public T getObject() {
        return obj;
    }

    public String toString() {
        if (obj == null)
            return "Node has no object assigned.";
        else
            return "Node contains: " + obj.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (!(other instanceof GNode<?>))
            return false;

        // If the objects match then they are equal
        return obj.equals(((GNode<?>) other).getObject());
    }

    @Override
    public int hashCode() {
        if (obj == null)
            return 0;
        return obj.hashCode();
    }
}
