import org.junit.Test;

import static org.junit.Assert.*;

public class TestCalculator {

    
    @Test
    public void infixToPostfix_null_ReturnsNull() {
        Queue inQueue = null;
        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertNull(outQueue);
    }
    

    
    @Test
    public void infixToPostfix_OneParameter_ReturnsOne() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(1.0f));
        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("1.0", outQueue.toString());
    }
    

    @Test
    public void infixToPostfix_OnePlusTwo() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(1.0f));
        inQueue.add(new Token("+"));
        inQueue.add(new Token(2.0f));
        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("1.0 2.0 +", outQueue.toString());
    }
    

    @Test
    public void infixToPostfix_OnePlusTwoPlusThree() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(1.0f));
        inQueue.add(new Token("+"));
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token("+"));
        inQueue.add(new Token(3.0f));
        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("1.0 2.0 + 3.0 +", outQueue.toString());
    }
    


    @Test
    public void infixToPostfix_ThreeMinusOne() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(3.0f));
        inQueue.add(new Token("-"));
        inQueue.add(new Token(1.0f));
        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("3.0 1.0 -", outQueue.toString());
    }
    
    @Test
    public void infixToPostfix_ThreeMinusOnePlusTwo() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(3.0f));
        inQueue.add(new Token("-"));
        inQueue.add(new Token(1.0f));
        inQueue.add(new Token("+"));
        inQueue.add(new Token(2.0f));

        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("3.0 1.0 - 2.0 +", outQueue.toString());
    }
    
    @Test
    public void infixToPostfix_TwoDivideThreeTimesFour() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token("/"));
        inQueue.add(new Token(3.0f));
        inQueue.add(new Token("*"));
        inQueue.add(new Token(4.0f));

        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("2.0 3.0 / 4.0 *", outQueue.toString());
    }
    
    @Test
    public void infixToPostfix_AddAndMultiplyWithParens() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token("("));
        inQueue.add(new Token(1.0f));
        inQueue.add(new Token("+"));
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token(")"));
        inQueue.add(new Token("*"));
        inQueue.add(new Token(3.0f));

        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("1.0 2.0 + 3.0 *", outQueue.toString());
    }
    
    //"2 / ( 3 + 1 ) * ( 4 * 2 )"), THelper.listCreator("2 3 1 + / 4 2 * *") }
    
    @Test
    public void infixToPostfix_AddDivideAndMultiplyWithParens() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token("/"));
        inQueue.add(new Token("("));
        inQueue.add(new Token(3.0f));
        inQueue.add(new Token("+"));
        inQueue.add(new Token(1.0f));
        inQueue.add(new Token(")"));
        inQueue.add(new Token("*"));
        inQueue.add(new Token("("));
        inQueue.add(new Token(4.0f));
        inQueue.add(new Token("*"));
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token(")"));

        Queue outQueue = new Calculator().infixToPostfix(inQueue);
        assertEquals("2.0 3.0 1.0 + / 4.0 2.0 * *", outQueue.toString());
    }  
    
    @Test
    public void eval_OnePlusTwo() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(1.0f));
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token("+"));
        float result = new Calculator().evaluate(inQueue);
        assertEquals(3.0, result, 0.1);
    }
    @Test
    public void eval_TwoMinusFour() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token(4.0f));
        inQueue.add(new Token("-"));
        float result = new Calculator().evaluate(inQueue);
        assertEquals(-2.0, result, 0.1);
    }
    
    //1 2 3 * +
    @Test
    public void eval_MutlipliesAndPlus() {
        Queue inQueue = new DSQueue();
        inQueue.add(new Token(1.0f));
        inQueue.add(new Token(2.0f));
        inQueue.add(new Token(3.0f));
        inQueue.add(new Token("*"));
        inQueue.add(new Token("+"));
        float result = new Calculator().evaluate(inQueue);
        assertEquals(7.0, result, 0.1);
    }
}
