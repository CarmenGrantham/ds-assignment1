import java.util.EmptyStackException;

/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class DSStack extends Stack {

    public DSStack() {
        list = new DSList();
    }

    public DSStack(Stack other) {
        list = new DSList((DSList)other.list);
    }

    public Token push(Token obj) {
        // Add token to top of stack, ie position 0
        list.add(0, obj);
        return obj;
    }

    public Token peek() {
        if (empty()) {
            throw new EmptyStackException();
        }
        // Get token at top of the stack, ie position 0
        return list.get(0);
    }

    public Token pop() {
        if (empty()) {
            throw new EmptyStackException();
        }
        // Remove token at top of the stack (position 0) and return it
        return list.remove(0);
    }

    public boolean empty() {
        return list.isEmpty();
    }

    public boolean add(Token obj) {
        // Add item to end of stack (to satisfy tests)
        // Collections doesn't specify where item should be added
        return list.add(obj);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int size() {
        return list.size();
    }

    @Override
    public String toString() {
        return list.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (this.getClass() == other.getClass()) {
            DSStack stack = (DSStack) other;
            return this.list.equals(stack.list);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 11 * list.hashCode();
    }

}
