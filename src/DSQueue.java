import java.util.NoSuchElementException;

/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class DSQueue extends Queue {
    
    public DSQueue(Queue input) {
        list = new DSList((DSList)input.list);
    }

    public DSQueue() {
        list = new DSList();
    }

    public boolean offer(Token t) {
        // Cannot offer null objects
        if (t == null) {
            throw new NullPointerException();
        }
        // Add item to end of list
        return list.add(t);
    }

    public Token poll() {
        // Cannot get item when queue is empty
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        
        // Remove the first item in the queue
        return list.remove(0);
    }

    public Token peek() {
        // Cannot get item on empty queue
        if (isEmpty()) {
            return null;
        }
        // Return the first item in the queue
        return list.get(0);
    }

    public boolean add(Token obj) {
        return offer(obj);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int size() {
        return list.size();
    }

    @Override
    public String toString() {
        return list.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (this.getClass() == o.getClass()) {
            DSQueue queue = (DSQueue) o;
            return this.list.equals(queue.list);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 7 * list.hashCode();
    }
}
