/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class DSList implements List {

    public Node head;

    public DSList() {
    }

    public DSList(Node head_) {
        head = head_;
    }

    public DSList(DSList other) { // Copy constructor.
        // Perform a deep copy of the other object
        if (other != null && other.head != null) {
            head = createNode(other.head);
        }
    }
    
    private Node createNode(Node other) {
        // Create a deep copy of node  
        if (other != null) {
            Node newNode = new Node(null, null, other.getObject());
            if (other.next != null) {
                newNode.next = createNode(other.next);
                newNode.next.previous = newNode;
            }
            return newNode;
        }
        return null;
    }

    public Token remove(int index) {
        // must provide value index or else exception is thrown
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) { // At top of the list
            Node deletedNode = head;
            if (head.next != null) {
                head.next.previous = head.previous;
            }
            head = head.next;
            return deletedNode.getObject();
        } else {
            Node node = getNode(index);
            node.previous.next = node.next;
            if (node.next != null) {
                node.next.previous = node.previous;
            }
            return node.getObject();
        }
    }

    public int indexOf(Token obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        
        // Loop through list until token can be found.
        // When found return index of token, where 0 is first item in list
        int nodePosition = 0;
        Node node = head;
        while (node != null) {
            if (node.getObject().equals(obj)) {
                return nodePosition;
            }
            // Go to next item
            node = node.next;
            nodePosition++;
        }
        return -1;
    }

    public Token get(int index) {
        // Must provide valid index to avoid exception being thrown
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        return getNode(index).getObject();
    }

    private Node getNode(int index) {
        // Find the node at the specified index, where first item is 0, second
        // item 1, etc
        int nodePosition = 0;
        Node node = head;
        while (node != null) {
            if (nodePosition == index) {
                return node;
            }
            node = node.next;
            nodePosition++;
        }
        return null;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public int size() {
        int count = 0;

        // Loop through all items in the list and use a counter 
        // to keep track of it's size.
        Node node = head;
        while (node != null) {
            count++;
            node = node.next;
        }

        return count;
    }

    @Override
    public String toString() {
        String text = "";

        if (head != null) {
            Node node = head;
            text += node.getObject();
            while (node.next != null) {
                node = node.next;
                text += " ";

                text += node.getObject(); // Don't use Node's toString, it has
                                          // extra text
            }
        }

        return text;
    }

    public boolean add(Token obj) {
        // Cannot add null objects
        if (obj == null) {
            throw new NullPointerException();
        }
        
        // Need to add object to end of the list

        // Get the last node
        Node lastNode = getLastNode();

        // Create new node and add it to the end of the list
        Node newNode = new Node(null, lastNode, obj);

        // Link node, either to head if null, or after the current last node
        if (head == null) {
            head = newNode;
        } else {
            lastNode.next = newNode;
        }

        return true;
    }

    public boolean add(int index, Token obj) {
        // Cannot add null objects
        if (obj == null) {
            throw new NullPointerException();
        }
        
        // Cannot have an index that is more than size + 1, or less
        // than 0
        if (index > size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        
        if (head == null) {
            head = new Node(null, null, obj);
        } else if (index == 0) {
            // Add item to head of list, moving current head down 1 position
            head = new Node(head, null, obj);
            if (head.next != null) {
                head.next.previous = head;
            }
        } else {
            Node node = getNode(index);
            if (node == null) {
                // if node at index couldn't be found add it to end of the list
                node = getLastNode();

                // Add item after node
                Node newNode = new Node(null, node, obj);
                node.next = newNode;
            } else {
                // Add item before node at specified index
                Node newNode = new Node(node, node.previous, obj);
                node.previous.next = newNode;
                node.previous = newNode;
            }

        }

        return true;
    }

    private Node getLastNode() {
        return getNode(size() - 1);
    }

    public boolean contains(Token obj) {
        return indexOf(obj) >= 0;
    }

    public boolean remove(Token obj) {
        // Cannot remove null object
        if (obj == null) {
            throw new NullPointerException();
        }
        int index = indexOf(obj);
        if (index < 0) {
            // couldn't find object so return false
            return false;
        }
        Token token = remove(index);
        return token != null;
    }

    @Override
    public int hashCode() {
        if (head != null) {
            return 31 * head.hashCode();
        }
        return 298399;     // No content so use a random number
    }

    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (this.getClass() == other.getClass()) {
            // There is a match when list size is the same and all items within
            // the list are exactly the same, including object order
            DSList list = (DSList) other;

            if (list.size() != this.size()) {
                return false;
            }
            for (int i = 0; i < this.size(); i++) {
                Token thisToken = this.get(i);
                Token otherToken = list.get(i);
                if (!thisToken.equals(otherToken)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
