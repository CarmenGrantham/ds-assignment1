import java.util.HashMap;
import java.util.Map;

public class MarksMap {

	private static final MarksMap map = new MarksMap();
	private Map<String, Float> marks = new HashMap<String, Float>();
	private Map<String, Float> obtainedMarks = new HashMap<String, Float>();
	
	/*
	 * Private constructor to avoid non-singleton use. 
	 */
	private MarksMap() {
		// 5
		marks.put("Token",  	0f);
			marks.put("Token:Equals", 0f);
			marks.put("Token:ToString", 0f);
            marks.put("Token:GetOperand", 0f);
            marks.put("Token:TokenFloat", 0f);
            marks.put("Token:TokenInt", 0f);
            marks.put("Token:HashCode", 0f);
            marks.put("Token:GetOperator", 0f);
            marks.put("Token:GetPrecedence", 0f);
            marks.put("Token:TokenString", 0f);

        marks.put("Node",  0f);
        	marks.put("Node:ToString", 0f);
        	marks.put("Node:EqualsObject", 0f);
        	marks.put("Node:HashCode", 0f);
		
		// We have an extra half a mark for this set of tests. 
		marks.put("List", 		25f);
			marks.put("List:List_Node", 				1f);
			marks.put("List:List_CopyConstructor", 		5f);
            marks.put("List:RemoveIntBeginning", 		2f);
            marks.put("List:RemoveIntEnd", 				2f);
            marks.put("List:RemoveInt",                	1f);
            marks.put("List:IndexOf",                   3f);
            marks.put("List:Get", 						2f);
            marks.put("List:AddIntToken",               2f);
            marks.put("List:Contains",                  2f);
            marks.put("List:RemoveToken_Beginning", 	2f);
            marks.put("List:RemoveToken_End", 			1f);
            marks.put("List:RemoveToken_Middle", 		1f);
            marks.put("List:RemoveToken_Single",      	1f);
     
            // 10
		marks.put("List:CollectionInterface", 15f);
			marks.put("ListCollectionInterface:AddNullPointer",   2f);
			marks.put("ListCollectionInterface:Add",              2f);
			marks.put("ListCollectionInterface:HashCodeEquality", 1f);
		    marks.put("ListCollectionInterface:Size",             0f);
		    marks.put("ListCollectionInterface:DSSize",           2f);
		    marks.put("ListCollectionInterface:Equality",         4f);
		    marks.put("ListCollectionInterface:ToString",         2f);
		    marks.put("ListCollectionInterface:IsEmpty",          2f);
    		
		marks.put("Queue",		5f);
            marks.put("Queue:Peek",                  1f);
            marks.put("Queue:Poll",                  1f);
            marks.put("Queue:Offer",                 1f);
            marks.put("Queue:Queue",                 0f);
            marks.put("Queue:Queue_CopyConstructor", 2f);

    	marks.put("QueueCollectionInterface", 5f);
    	    marks.put("QueueCollectionInterface:HashCodeEquality", 	1f);	
    	    marks.put("QueueCollectionInterface:Size", 				0.5f);
    	    marks.put("QueueCollectionInterface:Equality", 			1f);
    	    marks.put("QueueCollectionInterface:AddNullPointer", 	0.5f);
    	    marks.put("QueueCollectionInterface:Add", 				0.5f);
    	    marks.put("QueueCollectionInterface:IsEmpty", 			0.5f);	
    	    marks.put("QueueCollectionInterface:ToString", 			1f);
    			
        marks.put("Stack",		4.5f);
            marks.put("Stack:Pop",                   1f);
            marks.put("Stack:Peek",                  1f);
            marks.put("Stack:Push",                  1f);
            marks.put("Stack:Empty",                 0.5f);
            marks.put("Stack:Stack_CopyConstructor", 1f);

		marks.put("StackCollectionInterface", 5.5f);
		    marks.put("StackCollectionInterface:HashCodeEquality", 	1f);	
		    marks.put("StackCollectionInterface:Size", 				1f);
		    marks.put("StackCollectionInterface:Equality", 			1f);
		    marks.put("StackCollectionInterface:AddNullPointer", 	0.5f);
		    marks.put("StackCollectionInterface:Add", 				0.5f);
		    marks.put("StackCollectionInterface:IsEmpty", 			0.5f);
		    marks.put("StackCollectionInterface:ToString", 			1f);
		
		marks.put("InfixToPostfix",   15f);
			for ( int i = 0 ; i < 11 ; ++i )
				marks.put("InfixToPostfix:infixToPostfix[" + i + "]", 15.f/11.f);
		
		marks.put("PostfixEvaluation", 	15f);
			for ( int i = 0 ; i < 11 ; ++i ) 
				marks.put("PostfixEvaluation:postfixEvaluation[" + i + "]", 15.f / 11.f);
		// 100 marks total.
 
		marks.put("Generics:Queue:Collection", (1.5f));
        marks.put("GCollection$GQueueCollectionInterface:Equals", 1.5f/7f);
        marks.put("GCollection$GQueueCollectionInterface:ToString", 1.5f/7f);
        marks.put("GCollection$GQueueCollectionInterface:HashCode", 1.5f/7f);
        marks.put("GCollection$GQueueCollectionInterface:AddNullPointer", 1.5f/7f);
        marks.put("GCollection$GQueueCollectionInterface:IsEmpty", 1.5f/7f);
        marks.put("GCollection$GQueueCollectionInterface:Size", 1.5f/7f);
        marks.put("GCollection$GQueueCollectionInterface:Add", 1.5f/7f);
        
		marks.put("Generics:Stack:Collection", (1.5f));
        marks.put("GCollection$GStackCollectionInterface:Equals", 1.5f/7f);
        marks.put("GCollection$GStackCollectionInterface:ToString", 1.5f/7f);
        marks.put("GCollection$GStackCollectionInterface:HashCode", 1.5f/7f);
        marks.put("GCollection$GStackCollectionInterface:AddNullPointer", 1.5f/7f);
        marks.put("GCollection$GStackCollectionInterface:IsEmpty", 1.5f/7f);
        marks.put("GCollection$GStackCollectionInterface:Size", 1.5f/7f);
        marks.put("GCollection$GStackCollectionInterface:Add", 1.5f/7f);

        marks.put("Generics:List", 6f);
        marks.put("GList:Contains", 3f/16f);
        marks.put("GList:Get", 3f/16f);
        marks.put("GList:IndexOf", 3f/16f);
        marks.put("GList:IndexOf_invalidIndex", 3f/16f);
        marks.put("GList:RemoveInt", 3f/16f);
        marks.put("GList:AddIntT_IndexTooHigh", 3f/16f);
        marks.put("GList:AddIntT_IndexTooLow", 3f/16f);
        marks.put("GList:AddIntT_Null", 3f/16f);
        marks.put("GList:AddIntT_Null_ListNonEmpty", 3f/16f);
        marks.put("GList:AddIntT", 3f/16f);
        marks.put("GList:ContainsNull", 3f/16f);
        marks.put("GList:RemoveT", 3f/16f);
        marks.put("GList:RemoveTNull", 3f/16f);
        marks.put("GList:RemoveInt_IndexHigh", 3f/16f);
        marks.put("GList:RemoveInt_IndexLow", 3f/16f);
        marks.put("GList:BackwardsTraverse", 3f);
        marks.put("GList:AddIntToken_InsertAtBeginning", 3f/16f);
        
        marks.put("Generics:List:Collection", (1f));
        marks.put("GCollection$GListCollectionInterface:Equals", 1/7f);
        marks.put("GCollection$GListCollectionInterface:ToString", 1/7f);
        marks.put("GCollection$GListCollectionInterface:HashCode", 1/7f);
        marks.put("GCollection$GListCollectionInterface:AddNullPointer", 1/7f);
        marks.put("GCollection$GListCollectionInterface:IsEmpty", 1/7f);
        marks.put("GCollection$GListCollectionInterface:Size", 1/7f);
        marks.put("GCollection$GListCollectionInterface:Add", 1/7f);
        
    }
	
	public static MarksMap getInstance() {
		return map;
	}
	
	public float getMark(String name) {
		if ( !marks.containsKey(name) ) {
			if ( !name.contains("Failure") ) {
				System.out.print(name + " is not contained in the map: ");
			}
			return 0;
		}
		return marks.get(name);
	}
	
	public void addMarks(String key, Float mark) {
		marks.put(key, mark);
	}
	
	public void gainMark(String key, Float mark) {
		obtainedMarks.put(key, mark);
	}

}
