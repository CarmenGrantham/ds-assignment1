import static org.junit.Assert.*;

import org.junit.Test;

public class TestGDSList {

    @Test
    public void addAnItem() {
        GList<String> list = new GDSList<String>();
        list.add("mary");
        assertEquals("mary", list.toString());
    }
    

    
    @Test
    public void removeAtIndex0_1item() {
        GList<String> list = new GDSList<String>();
        list.add("mary");
        String removedItem = list.remove(0);
        assertTrue("mary".equals(removedItem));
    }
    @Test
    public void removeAtIndex0_2items() {
        GList<String> list = new GDSList<String>();
        list.add("mary");
        list.add("jane");

        assertEquals("mary jane", list.toString());
        
        String removedItem = list.remove(0);
        assertTrue("mary".equals(removedItem));
    }
    

    @Test
    public void traverseList() {
        GDSList<String> list = new GDSList<String>();
        list.add("mary");
        list.add("jane");
        list.add("paul");

        assertEquals("mary jane paul", list.toString());
        
        GNode<String> node = list.head;
        assertEquals("mary", node.getObject());
        
        int index = 1;
        while(node.next != null) {
            node = node.next;
            if (index == 1) {
                assertEquals("jane", node.getObject());
            } else {
                assertEquals("paul", node.getObject());
            }
            index++;
        }        
    }
    

    @Test
    public void traverseListInReverse() {
        GDSList<String> list = new GDSList<String>();
        list.add("mary");
        list.add("jane");
        list.add("paul");

        assertEquals("mary jane paul", list.toString());
        
        // Go all the way to the end of the list
        GNode<String> node = list.head;
        
        while(node.next != null) {
            node = node.next;
        }
        assertEquals("paul", node.getObject());
        
        // Then use previous to go backwards
        int index = 1;
        while(node.previous != null) {
            node = node.previous;
            if (index == 1) {
                assertEquals("jane", node.getObject());
            } else {
                assertEquals("mary", node.getObject());
            }
            index++;
        }        
    }
}
