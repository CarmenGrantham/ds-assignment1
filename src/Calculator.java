/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class Calculator {

    // Store the output of conversions
    Queue postfix;

    // Store the operators during conversion
    Stack operatorStack;

    public Queue infixToPostfix(Queue infix) {
        if (infix == null) {
            return null;
        }
        postfix = new DSQueue();

        operatorStack = new DSStack();

        while (!infix.isEmpty()) {
            // Get item and remove it from the queue
            Token token = infix.poll();
            if (token.isOperand()) {
                // Add number to output
                postfix.add(token);
            } else if (token.isOperator()) {
                handleOperator(token);
            } else if (token.isParen()) {
                if (token.getOperator().equals("(")) {
                    // Add'(' to stack
                    operatorStack.push(token);
                } else {
                    handleRightParentheses(token);
                }
            }
        }

        // Process any tokens left on the stack
        while (!operatorStack.empty()) {
            Token token = operatorStack.pop();
            if (token.isParen()) {
                // There is an unmatched parentheses so statement is invalid
                throw new IllegalArgumentException();
            } else {
                postfix.add(token);
            }
        }
        return postfix;
    }

    private void handleOperator(Token token) {
        if (operatorStack.isEmpty()) {
            operatorStack.push(token);
        } else {
            Token topToken = operatorStack.peek();
            if (topToken.isParen()
                    || token.getPrecedence() > topToken.getPrecedence()) {
                operatorStack.push(token);
            } else {
                while (!operatorStack.empty()
                        && token.getPrecedence() <= topToken.getPrecedence()) {
                    operatorStack.pop();
                    postfix.add(topToken);
                    if (!operatorStack.empty()) {
                        topToken = operatorStack.peek();
                    }
                }
                operatorStack.push(token);
            }
        }
    }

    private void handleRightParentheses(Token token) {
        boolean foundLeftParens = false;
        // Go through stack until left parentheses is found, adding all other
        // items to output queue
        while (!operatorStack.empty()) {
            Token topToken = operatorStack.pop();
            if (topToken.getOperator().equals("(")) {
                foundLeftParens = true;
                break;
            } else {
                postfix.add(topToken);
            }
        }

        if (!foundLeftParens) {
            // There is an unmatched parentheses so statement is invalid
            throw new IllegalArgumentException();
        }
    }

    public float evaluate(Queue input) {
        // Calculate the value of the provided postfix statement
        
        // cannot evaluate null input
        if (input == null) {
            return 0;
        }        

        operatorStack = new DSStack();
        
        while (!input.isEmpty()) {
            Token token = input.poll();
            if (token.isOperand()) {
                operatorStack.push(token);
            } else {
                // The top of the stack is the right hand side of the equation
                // and the next one is the left hand side. Use these along
                // with the current token to get a result.
                Token rhs = operatorStack.pop();
                Token lhs = operatorStack.pop();
                if (rhs != null && lhs != null) {
                    float rhsValue = rhs.getOperand();
                    float lhsValue = lhs.getOperand();
                    float result = 0;
                            
                     // Evaluate the operator
                    switch (token.getOperator()) {
                    case "+": 
                        result = lhsValue + rhsValue;
                        break;
                    case "-": 
                        result = lhsValue - rhsValue;
                        break;
                    case "/": 
                        result = lhsValue / rhsValue;
                        break;
                    case "*": 
                        result = lhsValue * rhsValue;
                        break;        
                    }
                    
                    // Add result back on to stack
                    operatorStack.push(new Token(result));
                }
                
            }
        }
        // Get the result on the operatorStack
        Token tokenTop = operatorStack.pop();
        if (tokenTop == null) {
            // There is nothing in the stack, which should not happen, but just in case
            return 0;
        } else {
            if (!operatorStack.empty() || !tokenTop.isOperand()) {
                // There shouldn't be anything left on the stack - the
                // queue is not valid
                // If top token is not a number - queue is invalid
                // - this might occur when queue just has a '+'
                throw new IllegalArgumentException();
            }
            return tokenTop.getOperand();
        }
    }

}
