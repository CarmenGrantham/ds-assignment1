/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class Token {

    public enum Type {
        OPERATOR, OPERAND, PAREN
    };

    
    private static final String OPERATORS = "+-*/^()";
    private static final int[] PRECEDENCE = {0, 0, 1, 1, 2, 3, 3};
    
    public Type type;

    private String operator;
    private float operand;

    private int precedence;

    public Token(float op) {
        type = Type.OPERAND;
        operand = op;
    }

    public Token(String op) {
        operator = op;
        // If this operator is a parenthesis then set Type to PAREN
        if (op.equals("(") || op.equals(")")) {
            type = Type.PAREN;
        } else {
            type = Type.OPERATOR;
        }
        // Use PRECEDENCE and OPERATOR arrays to determine precedence
        precedence = PRECEDENCE[OPERATORS.indexOf(op)];
    }

    public Token(Token other) {
        type = other.type;
        operator = other.getOperator();
        precedence = other.getPrecedence();
        operand = other.getOperand();
    }

    public String getOperator() {
        return operator;
    }

    public float getOperand() {
        return operand;
    }

    public int getPrecedence() {
        return precedence;
    }
    
    public boolean isOperator() {
        return this.type == Type.OPERATOR;
    }
    
    public boolean isOperand() {
        return this.type == Type.OPERAND;
    }
    
    public boolean isParen() {
        return this.type == Type.PAREN;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (this.getClass() == obj.getClass()) {
            Token token = (Token) obj;
            // objects are the same if operand and operator match
            return token.getOperand() == getOperand()
                    && (token.getOperator() == null ? getOperator() == null
                            : token.getOperator().equals(getOperator()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        // Use a hashcode of 1 + operand's hashcode + operators hashcode
        // (if not null)
        int hash = 1;
        hash += new Float(operand).hashCode();
        hash += (operator == null ? 0 : operator.hashCode());
        return hash;
    }

    public String toString() {
        if (type == Type.OPERAND) {
            // Use operand (float) converted to a string
            return "" + getOperand();
        } else {
            return getOperator();
        }
    }
}
