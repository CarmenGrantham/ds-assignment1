import java.util.EmptyStackException;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestDSStack {

    @Test(expected=NullPointerException.class)
    public void testPush_NullToken() {
        DSStack stack = new DSStack();
        Token token = stack.push(null);
    }
    
    @Test
    public void testPush_OneToken() {
        DSStack stack = new DSStack();
        Token zeroToken = new Token(0);
        Token token = stack.push(zeroToken);
        assertEquals(1, stack.size());
        assertTrue(zeroToken.equals(token));
    }
    
    @Test
    public void testSize_NoTokens() {
        DSStack stack = new DSStack();
        assertEquals(0, stack.size());
    }

    @Test
    public void testSize_OneToken() {
        DSStack stack = new DSStack();
        stack.push(new Token(0));
        assertEquals(1, stack.size());
    }

    @Test
    public void testSize_ManyTokens() {
        DSStack stack = new DSStack();
        stack.push(new Token(0));
        stack.add(new Token(1));
        stack.push(new Token(2));
        assertEquals(3, stack.size());
    }


    @Test(expected=EmptyStackException.class)
    public void testPeek_NoTokens() {
        DSStack stack = new DSStack();
        Token token = stack.peek();
    }

    @Test
    public void testPeek_OneToken() {
        DSStack stack = new DSStack();
        stack.push(new Token(0));
        Token token = stack.peek();
        
        assertNotNull(token);;
        assertTrue(token.equals(new Token(0)));
        assertEquals(1, stack.size());
    }
    @Test
    public void testPeek_ManyTokens() {
        DSStack stack = new DSStack();
        stack.push(new Token(0));
        stack.push(new Token(1));
        stack.push(new Token(2));
        Token token = stack.peek();
        
        assertNotNull(token);
        assertTrue(token.equals(new Token(2)));
        assertEquals(3, stack.size());
        
        
        stack.pop();
        
        token = stack.peek();
        
        assertNotNull(token);
        assertTrue(token.equals(new Token(1)));
        assertEquals(2, stack.size());
        
    }
    
    @Test(expected=EmptyStackException.class)
    public void testPop_NoTokens() {
        DSStack stack = new DSStack();
        stack.pop();
    }
    

    @Test
    public void testPop_OneToken() {
        DSStack stack = new DSStack();
        stack.push(new Token(0));
        assertEquals(1, stack.size());
        
        Token token = stack.pop();
        assertEquals(0, stack.size());
        assertTrue(token.equals(new Token(0)));
    }
    
    @Test
    public void testCopyConstructor_NoItems(){
        DSStack stack = new DSStack();
        
        DSStack other = new DSStack(stack);
        
        assertEquals(0, other.size());
        
    }

    @Test
    public void testCopyConstructor_TwoItems(){
        DSStack stack = new DSStack();
        stack.add(new Token(0));
        stack.add(new Token(1));
        
        DSStack other = new DSStack(stack);
        
        assertEquals(2, other.size());
        assertTrue(other.peek().equals(stack.peek()));
    }
    

    @Test
    public void testCopyConstructor() {
        
        /* The copy constructor implementation requires a deep copy. 
         * That is, after copying the object changes to the original should not impact the copied object.
         */
        Stack s = new DSStack();
        s.add(new Token(0));
        s.add(new Token(1));
        
        Stack other = new DSStack(s);
        
        assertEquals(2, other.size());
        for ( int i = 0 ; i < 2 ; ++i ) {
            assertEquals(new Token(i), other.pop());
        }

        
        s.push(new Token(2));
        assertEquals(0, other.size());
        
    }
    
    @Test
    public void testToString_NoTokens() {
        Stack stack = new DSStack();
        assertEquals("", stack.toString());
    }
    

    @Test
    public void testToString_OneItem(){
        Stack stack = new DSStack();
        stack.push(new Token(0));
        assertEquals("0.0", stack.toString());
    }
    

    @Test
    public void testToString_ThreeItems(){
        Stack stack = new DSStack();
        stack.push(new Token(0));
        stack.push(new Token(1));
        stack.push(new Token(2));
        assertEquals("2.0 1.0 0.0", stack.toString());
    }
    

    @Test
    public void testToString_WithPushesAndPops(){
        Stack stack = new DSStack();
        stack.push(new Token(0));
        stack.push(new Token(1));
        stack.push(new Token(2));
        assertEquals("2.0 1.0 0.0", stack.toString());
        
        stack.pop();
        assertEquals("1.0 0.0", stack.toString());

        stack.peek();
        assertEquals("1.0 0.0", stack.toString());
    }
    
}
