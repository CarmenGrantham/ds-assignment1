import static org.junit.Assert.*;

import org.junit.Test;

public class ListCollectionInterfaceTest extends CollectionsTest<DSList> {

	@Override
	protected DSList createInstance() {
		return new DSList();
	}
	
	@Test
    public void testDSSize() {
    	Node node1 = new Node(null, null, new Token(3));
    	Node node2 = new Node(null, node1, new Token(2));
    	Node node3 = new Node(null, node2, new Token(1));
    	node1.next = node2;
    	node2.next = node3;
    	DSList l = new DSList(node1);
    	
    	assertEquals("Size method should reflect the number of nodes in the list.", 3, l.size());
    	l = new DSList();
    	assertEquals("Size method should reflect the number of nodes in the list.", 0, l.size());
    }

}
