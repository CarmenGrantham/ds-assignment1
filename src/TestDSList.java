import static org.junit.Assert.*;

import org.junit.Test;


public class TestDSList {

    @Test
    public void constructDSList() {
        DSList list = new DSList();
        assert(list.isEmpty());
    }
    
    @Test
    public void isEmpty_True() {
        DSList list = new DSList();
        assert(list.isEmpty());
    }
    
    @Test
    public void isEmpty_False() {
        DSList list = new DSList();
        list.add(new Token(0));
        assertFalse(list.isEmpty());
    }
    
    @Test 
    public void toString_3items() {
        List list = new DSList();
        list.add(new Token(0));
        list.add(new Token(1));
        list.add(new Token(2));
        assertEquals("0.0 1.0 2.0", list.toString());

    }
    
    @Test
    public void addAtIndex0_1item() {
        List list = new DSList();
        list.add(0, new Token(0));
        assertEquals("0.0", list.toString());
    }
    
    @Test
    public void addAtIndex0_2items() {
        List list = new DSList();
        list.add(0, new Token(0));
        list.add(0, new Token(1));
        assertEquals("1.0 0.0", list.toString());
    }
    

    @Test
    public void addAtIndex1_3items() {
        List list = new DSList();
        list.add(0, new Token(0));
        list.add(0, new Token(1));
        list.add(1, new Token(2));
        assertEquals("1.0 2.0 0.0", list.toString());
    }
    
    
    @Test
    public void addAtIndex2_3items() {
        List list = new DSList();        
        list.add(new Token(1));
        list.add(0, new Token(0));
        assertEquals("0.0 1.0", list.toString());
        
        assertEquals("List.add(int, Token) should insert the specified token at the specified index.", new Token(0), list.get(0));
        
        list.add(2, new Token(2));
        assertEquals("0.0 1.0 2.0", list.toString());
        assertEquals("List.add(int, Token) should insert the specified token at the specified index.", new Token(2), list.get(2));
        
        list.add(2, new Token(3));
        assertEquals("0.0 1.0 3.0 2.0", list.toString());
        assertEquals("List.add(int, Token) should insert the specified token at the specified index.", new Token(3), list.get(2));
        
        assertEquals(4, list.size());
    }
    
    @Test(expected=IndexOutOfBoundsException.class)
    public void removeAtIndex0_emptylist() {
        List list = new DSList();
        list.remove(0);
    }
    
    @Test(expected=IndexOutOfBoundsException.class)
    public void removeAtIndex1_1item() {
        List list = new DSList();      
        list.add(new Token(0));
        list.remove(1);
    }
    
    @Test
    public void removeAtIndex0_1item() {
        List list = new DSList();      
        list.add(new Token(0));
        Token token = list.remove(0);
        assertEquals("0.0", token.toString());
    }
    

    @Test
    public void removeAtIndex0_2items() {
        List list = new DSList();      
        list.add(new Token(0));  
        list.add(new Token(1));
        Token token = list.remove(0);
        assertEquals("0.0", token.toString());
        assertEquals(1, list.size());
    }
    

    @Test
    public void testRemoveIntEnd() {
        DSList l = new DSList();
        l.add(new Token("+"));
        l.add(new Token("-"));
        l.add(new Token("*"));
        assertEquals("+ - *", l.toString());

        Token t = l.remove(2);
        assertEquals("+ -", l.toString());
        assertNotNull("List.remove(int) should not return null when elements exist at that index in the list", t);
        assertEquals("Remove did not remove from the list correctly", new Token("*"), t);
        assertEquals("Remove did not remove from the list correctly", new Token("-"), l.remove(1));
        

        assertEquals(1, l.size());
    }

    
    @Test
    public void testList_CopyConstructor() {
        
        /* The copy constructor implementation requires a deep copy. 
         * That is, after copying the object changes to the original should not impact the copied object.
         */
        DSList s = new DSList();
        s.add(new Token(0));
        s.add(new Token(1));
        
        assertEquals("0.0 1.0", s.toString());
        
        DSList other = new DSList(s);
        assertEquals("0.0 1.0", other.toString());
        
        assertEquals("Copy constructor should create a List of equal size to the copied List", 2, other.size());
        for ( int i = 1 ; i >= 0 ; --i ) {
            assertEquals("Copy constructor did not copy the List correctly", new Token(i), other.remove(i));
        }
        
        s.add(new Token(2));
        assertEquals("Changes to original list should not impact the second list", 0, other.size());
        
    }
    
    @Test
    public void testEqualsTrue_BothEmpty() {
        DSList list1 = new DSList();
        DSList list2 = new DSList();
        assertTrue(list1.equals(list2));
        assertTrue(list2.equals(list1));
    }
    
    @Test
    public void testEqualsFalse_OneIsEmpty() {
        DSList list1 = new DSList();
        DSList list2 = new DSList();
        list2.add(new Token(0));
        
        assertFalse(list1.equals(list2));
        assertFalse(list2.equals(list1));
    }
    

    
    @Test
    public void testEqualsFalse_OneHasExtra2items() {
        DSList list1 = new DSList();
        list1.add(new Token(0));
        list1.add(new Token(1));
        list1.add(new Token(2));
        
        DSList list2 = new DSList();
        list2.add(new Token(0));
        
        assertFalse(list1.equals(list2));
        assertFalse(list2.equals(list1));
    }
    

    @Test
    public void testEqualsTrue_BothHave3Items() {
        DSList list1 = new DSList();
        list1.add(new Token(0));
        list1.add(new Token(1));
        list1.add(new Token(2));
        
        DSList list2 = new DSList();
        list2.add(new Token(0));
        list2.add(new Token(1));
        list2.add(new Token(2));
        
        assertTrue(list1.equals(list2));
        assertTrue(list2.equals(list1));
    }


    @Test
    public void testEqualsFalse_BothHave3DifferentItems() {
        DSList list1 = new DSList();
        list1.add(new Token(10));
        list1.add(new Token(11));
        list1.add(new Token(12));
        
        DSList list2 = new DSList();
        list2.add(new Token(0));
        list2.add(new Token(1));
        list2.add(new Token(2));
        
        assertFalse(list1.equals(list2));
        assertFalse(list2.equals(list1));
    }
}
