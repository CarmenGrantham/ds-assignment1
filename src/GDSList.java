/**
 * 
 */

/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class GDSList<T> implements GList<T> {
    public GNode<T> head;

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#add(java.lang.Object)
     */
    @Override
    public boolean add(T obj) {
        // Cannot add null objects
        if (obj == null) {
            throw new NullPointerException();
        }
        
        // Need to add object to end of the list

        // Get the last node
        GNode<T> lastNode = getLastNode();

        // Create new node and add it to the end of the list
        GNode<T> newNode = new GNode<T>(null, lastNode, obj);

        // Link node, either to head if null, or after the current last node
        if (head == null) {
            head = newNode;
        } else {
            lastNode.next = newNode;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return head == null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#size()
     */
    @Override
    public int size() {
        int count = 0;

        // Loop through all items in the list and use a counter 
        // to keep track of it's size.
        GNode<T> node = head;
        while (node != null) {
            count++;
            node = node.next;
        }

        return count;
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GList#add(int, java.lang.Object)
     */
    @Override
    public boolean add(int index, T obj) {
        // Cannot add null objects
        if (obj == null) {
            throw new NullPointerException();
        }
        
        // Cannot have an index that is more than size + 1, or less
        // than 0
        if (index > size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        
        if (head == null) {
            head = new GNode<T>(null, null, obj);
        } else if (index == 0) {
            // Add item to head of list, moving current head down 1 position
            head = new GNode<T>(head, null, obj);
            if (head.next != null) {
                head.next.previous = head;
            }
        } else {
            GNode<T> node = getGNode(index);
            if (node == null) {
                // if node at index couldn't be found add it to end of the list
                node = getLastNode();

                // Add item after node
                GNode<T> newNode = new GNode<T>(null, node, obj);
                node.next = newNode;
            } else {
                // Add item before node at specified index
                GNode<T> newNode = new GNode<T>(node, node.previous, obj);
                node.previous.next = newNode;
                node.previous = newNode;
            }

        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GList#contains(java.lang.Object)
     */
    @Override
    public boolean contains(T obj) {
        return indexOf(obj) >= 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GList#remove(java.lang.Object)
     */
    @Override
    public boolean remove(T obj) {
        // Cannot remove null object
        if (obj == null) {
            throw new NullPointerException();
        }
        int index = indexOf(obj);
        if (index < 0) {
            // couldn't find object so return false
            return false;
        }
        T removedObj = remove(index);
        return removedObj != null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GList#remove(int)
     */
    @Override
    public T remove(int index) {
        // must provide value index or else exception is thrown
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) { // At top of the list
            GNode<T> deletedNode = head;
            if (head.next != null) {
                head.next.previous = head.previous;
            }
            head = head.next;
            return deletedNode.getObject();
        } else {
            GNode<T> node = getGNode(index);
            node.previous.next = node.next;
            if (node.next != null) {
                node.next.previous = node.previous;
            }
            return node.getObject();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GList#get(int)
     */
    @Override
    public T get(int index) {
        // Must provide valid index to avoid exception being thrown
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        return getGNode(index).getObject();
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GList#indexOf(java.lang.Object)
     */
    @Override
    public int indexOf(T object) {
        if (object == null) {
            throw new NullPointerException();
        }
        
        // Loop through list until token can be found.
        // When found return index of token, where 0 is first item in list
        int nodePosition = 0;
        GNode<T> node = head;
        while (node != null) {
            if (node.getObject().equals(object)) {
                return nodePosition;
            }
            // Go to next item
            node = node.next;
            nodePosition++;
        }
        return -1;
    }

    

    private GNode<T> getLastNode() {
        return getGNode(size() - 1);
    }

    private GNode<T> getGNode(int index) {
        // Find the node at the specified index, where first item is 0, second
        // item 1, etc
        int nodePosition = 0;
        GNode<T> node = head;
        while (node != null) {
            if (nodePosition == index) {
                return node;
            }
            node = node.next;
            nodePosition++;
        }
        return null;
    }
    
    @Override
    public String toString() {
        String text = "";

        if (head != null) {
            GNode<T> node = head;
            text += node.getObject();
            while (node.next != null) {
                node = node.next;
                text += " ";

                text += node.getObject(); // Don't use Node's toString, it has
                                          // extra text
            }
        }

        return text;
    }

    @Override
    public int hashCode() {
        if (head != null) {
            return 31 * head.hashCode();
        }
        return 298399;     // No content so use a random number
    }
    
    @Override
    public boolean equals(Object other) {

        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
            
        if (this.getClass() == other.getClass()) {
            // There is a match when list size is the same and all items within
            // the list are exactly the same, including object order            
            GDSList<T> list = (GDSList<T>) other;

            if (list.size() != this.size()) {
                return false;
            }
            for (int i = 0; i < this.size(); i++) {
                T thisObj = this.get(i);
                T otherObj = list.get(i);
                if (!thisObj.equals(otherObj)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
