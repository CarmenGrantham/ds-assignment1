import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

public class TestDSQueue {

    @Test(expected=NullPointerException.class)
    public void testOffer_nullObject() {
        Queue queue = new DSQueue();
        queue.offer(null);
    }
    
    @Test
    public void testIsEmptyTrue() {
        Queue queue = new DSQueue();
        assertTrue(queue.isEmpty());
    } 

    @Test
    public void testIsEmptyFalse_HasOneItem() {
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        assertFalse(queue.isEmpty());
    }
    

    @Test
    public void testIsEmptyTrue_HasOneItemAddedThenRemoved() {
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        assertFalse(queue.isEmpty());
        
        queue.poll();
        assertTrue(queue.isEmpty());
    }

    @Test(expected=NoSuchElementException.class)
    public void testPoll_HasNoItems() {
        Queue queue = new DSQueue();
        queue.poll();
    }
    
    @Test
    public void testPoll_HasTwoItemsOneRemoved() {
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        queue.offer(new Token(1));
        queue.poll();
        assertEquals(1, queue.size());
    }
    
    @Test
    public void testSizeReturnsZero(){
        Queue queue = new DSQueue();
        assertEquals(0, queue.size());
    }    
    
    @Test
    public void testSizeReturnsOne(){
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        assertEquals(1, queue.size());
    }
    
    
    @Test
    public void testSizeReturnsThree(){
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        queue.offer(new Token(1));
        queue.offer(new Token(2));
        assertEquals(3, queue.size());
    }
    
    
    
    @Test
    public void testToString_NoItems(){
        Queue queue = new DSQueue();
        assertEquals("", queue.toString());
    }
    
    @Test
    public void testToString_OneItem(){
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        assertEquals("0.0", queue.toString());
    }
    

    @Test
    public void testToString_ThreeItems(){
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        queue.offer(new Token(1));
        queue.offer(new Token(2));
        assertEquals("0.0 1.0 2.0", queue.toString());
    }
    

    @Test
    public void testToString_WithOffersAndPulls(){
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        queue.offer(new Token(1));
        queue.offer(new Token(2));
        assertEquals("0.0 1.0 2.0", queue.toString());
        
        queue.poll();
        assertEquals("1.0 2.0", queue.toString());

        queue.peek();
        assertEquals("1.0 2.0", queue.toString());
    }
    

    @Test(expected=IndexOutOfBoundsException.class)
    public void testPeek_WithNoItems(){
        Queue queue = new DSQueue();
        queue.peek();
    }

    @Test
    public void testPeek_WithItems(){
        Queue queue = new DSQueue();
        queue.offer(new Token(0));
        Token t = queue.peek();
        assertNotNull(t);
        assertTrue(t.equals(new Token(0)));
    }
    
    @Test
    public void testCopyConstructor_NoItems(){
        Queue s = new DSQueue();
        
        Queue other = new DSQueue(s);
        
        assertEquals(0, other.size());
        
    }

    @Test
    public void testCopyConstructor_TwoItems(){
        Queue s = new DSQueue();
        s.add(new Token(0));
        s.add(new Token(1));
        
        Queue other = new DSQueue(s);
        
        assertEquals(2, other.size());
    }
    

}
