import java.util.EmptyStackException;

/**
 * 
 */

/**
 * 
 * @author Carmen Grantham (gracy016 - Student ID: 110061791)
 *
 */
public class GDSStack<T> extends GStack<T> {


    public GDSStack() {
        list = new GDSList<T>();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#add(java.lang.Object)
     */
    @Override
    public boolean add(T obj) {
        // Add item to end of stack (to satisfy tests)
        // Collections doesn't specify where item should be added
        return list.add(obj);
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GCollection#size()
     */
    @Override
    public int size() {
        return list.size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GStack#empty()
     */
    @Override
    public boolean empty() {
        return list.isEmpty();
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GStack#peek()
     */
    @Override
    public T peek() {
        if (empty()) {
            throw new EmptyStackException();
        }
        // Get token at top of the stack, ie position 0
        return list.get(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GStack#pop()
     */
    @Override
    public T pop() {
        if (empty()) {
            throw new EmptyStackException();
        }
        // Remove token at top of the stack (position 0) and return it
        return list.remove(0);
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GStack#push(java.lang.Object)
     */
    @Override
    public T push(T obj) {
        // Add token to top of stack, ie position 0
        list.add(0, obj);
        return obj;
    }

    /*
     * (non-Javadoc)
     * 
     * @see Generics.GStack#toString()
     */
    @Override
    public String toString() {
        return list.toString();
    }

    @Override
    public int hashCode() {
        return 11 * list.hashCode();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (this.getClass() == other.getClass()) {
            GDSStack<T> stack = (GDSStack<T>) other;
            return this.list.equals(stack.list);
        }
        return false;
    }
}
